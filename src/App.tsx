import "./App.css";
import { FixedSizeList as List } from "react-window";
import axios from "axios";
import React,{ useEffect, useState } from "react";
import AutoSizer from 'react-virtualized-auto-sizer'
function App() {
  const [user, setUser] = useState([]as any)
  const listRef:any = React.createRef()

  useEffect(() => {
    fetchDataUser()
  },[])


  const fetchDataUser = async() => {
    await axios.get('https://jsonplaceholder.typicode.com/users')
    .then(response => {
      setUser(response.data)
    })
    .catch(err => console.log(err))
  }



  const Row = ({ index, style }: any) => (
    <div className={`${index % 2 === 0 ? 'listGenap' : ''}`} style={style}>
      <ul>
        <li>
        <h1>{user[index].name}</h1>
        </li>
      </ul>
    </div>
  );
  


  return (
    <div className='App'>
      <h1>Belajar Virtualize List with Typescript</h1>
      <div style={{width:'700px', height:'500px'}}>
      <AutoSizer>
        {({width, height}) => (
          <List className="List" height={height} width={width} itemSize={100} itemCount={user.length}>
        {Row}
        </List>
        )}
      </AutoSizer>
        </div>
    </div>
  );
}

export default App;
